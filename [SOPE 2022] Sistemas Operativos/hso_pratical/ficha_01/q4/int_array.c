#include <stdio.h>
int main() {
	int i;
	int primes[] = {2, 3, 5, 7, 11};
	for (i = 0; i < sizeof(primes)/sizeof(int); i++) {
		printf("%d <--> %d\n", primes[i], *(primes + i));
	}
	printf("sizeof(int) = %d bytes\n", (int) sizeof(int));
	
	return 0;
}
